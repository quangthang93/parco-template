$(function() {
  var $window = $(window);
  var winW = $window.width();
  var winH = $window.height();
  var scrollTop = $window.scrollTop();
  var startPos = 0;
  var $pageTop = $('.js-pagetop');
  var $shopList = $('.js-shop-list');
  var shopListPos = $shopList.offset().top;
  var $tabMenu = $('.js-tab-menu');
  // var deviceMode = winW > 768 ? 'pc' : 'sp';


  $pageTop.click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 300);
  });

  $window.on('load', function() {
    var salelistTitle = $('.salelist__title');
    var salelistTitleFlg;
    salelistTitle.on('click', function(e) {
      e.preventDefault();
      if ($(this).is('.is-open')) {
        $(this).removeClass('is-open');
        $(this).next().slideUp();
      } else {
        $(this).addClass('is-open');
        $(this).next().slideDown();
      }
    });
  });

  $window.on('load scroll', function() {
    scrollTop = $(window).scrollTop();

    if (scrollTop >= 100) {
      $pageTop.show();
    } else {
      $pageTop.hide();
    }
    if (scrollTop > startPos || scrollTop < shopListPos) {
      $tabMenu.removeClass('active');
    } else {
      $tabMenu.addClass('active');
    }
    startPos = scrollTop;
  });

  // Animation
  $(window).scroll(function() {
    // Scroll slide up out
    $('.ani-widthfull, .ani-fadein, .ani-widthfull2, .ani-fadein2, .ani-fadeinup').each(function() {
      var elemPos = $(this).offset().top;
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });
  });

  // Scroll to section
  $('a[href^="#"]').click(function() {
    var href= $(this).attr("href");
    var hash = href == "#" || href == "" ? 'html' : href;
    var position = $(hash).offset().top - 100;
    $('body,html').stop().animate({scrollTop:position}, 1000);
    return false;
  });


});